class Numbering:
    def __init__(self, dividend, divisor):
        self.dividend = dividend
        self.divisor = divisor
        # if type(self.dividend) != int:
        #     raise TypeError('The Dividend can only be number')
        # try:
        #     self.dividend
        #     self.divisor
        # except NameError:
        #     print('NO INPUTS PROVIDED BY YOU')
        if self.dividend is None or self.divisor is None:
            raise NameError('No Inputs provided')
        if not isinstance(self.dividend, float) and not isinstance(self.divisor, float):
            raise TypeError('Your Inputs must be numbers!')
        if type(self.divisor) != float:
            raise TypeError('The Divisor can only be number')
        if not isinstance(self.dividend, float):
            raise TypeError('The Dividend can only be number')



    def dividing(self):
        if self.divisor == 0:
            raise ZeroDivisionError('The Divisor cannot be zero')

        return self.dividend / self.divisor




