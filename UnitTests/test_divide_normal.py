import unittest
from Numbering import Numbering

class TestDivideNormal(unittest.TestCase):
    def setUp(self):
        self.number1 = Numbering(15.0, 3.0)
        self.number2 = Numbering(float(15), float(-3))

    def test_divide_instance(self):
        self.assertIsInstance(self.number1.dividend, float)



if __name__ == '__main__':
    unittest.main()
