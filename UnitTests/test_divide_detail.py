import unittest
from Numbering import Numbering

class MyOtherTestCase(unittest.TestCase):
    def setUp(self):
        self.number1 = Numbering(15.0, 3.0)
        self.number2 = Numbering(float(15), float(-3))
        self.number3 = Numbering(-15.0, 3.0)
        self.number4 = Numbering(0.0, 5.0)
        self.number5 = Numbering(5.0, 0.0)
        self.number6 = Numbering(1 / 2, 2.99)
            # self.number7 = Numbering()

    def test_divide_normal(self):
        self.assertAlmostEqual(self.number1.dividing(), 5, delta=0.0001)

    def test_divide_negative_divisor(self):
        # self.assertAlmostEqual(self.number2.dividing(), -5, delta=0.0001)
        # self.assertEqual(self.number2.dividing(), -5.0)
        print(self.number2.dividing())
        print(round(self.number6.dividing(), 2))
        print(self.number6.dividend)
        self.assertEqual(round(self.number6.dividing(), 2), 0.17)

    def test_divide_instance(self):
        self.assertIsInstance(self.number1.dividend, float)

    def test_divide_error_on_zero(self):
        with self.assertRaises(ZeroDivisionError):
            self.number5.dividing()

    def test_incorrect_input_type_dividend(self):
        with self.assertRaises(TypeError) as context:
            self.incorrect_input = Numbering("P", 1)
        print(context.exception)

    def test_incorrect_input_type_divisor(self):
        with self.assertRaises(TypeError) as context:
            self.incorrect_input1 = Numbering(1, "P")
        print(context.exception)

    def test_incorrect_input_type_divisor_and_divident(self):
        with self.assertRaises(TypeError) as context:
            self.incorrect_input1 = Numbering([1, 2, 3], "P")
        print(context.exception)

    def test_no_input(self):
        with self.assertRaises(NameError) as context:
            self.number7 = Numbering(None, 1)
        print(context.exception)


if __name__ == '__main__':
    unittest.main()
